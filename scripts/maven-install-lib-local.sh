#!/usr/bin/env bash
#
# Script para criação de repositório local com libs
# que não estão no repositório central do maven.
#
# Este script deve ser executado pelo menos uma vez sempre
# que for configurar o projeto do início.
#

if [[ ! -d lib-local ]]; then
    echo "Diretório 'lib-local' não encontrado. Baixe-o de <https://bitbucket.org/fidias/open-ecf/downloads/>"
    exit 1
fi

mvn install:install-file \
    -DgroupId=br.com.bemajava \
    -DartifactId=bematech-framework-jna \
    -Dversion=1.0 \
    -Dfile=lib-local/BematechFrameworkJNA.jar \
    -Dpackaging=jar \
    -DlocalRepositoryPath=lib-local

mvn install:install-file \
    -DgroupId=br.com.labs.daruma \
    -DartifactId=daruma-framework-jna \
    -Dversion=1.0 \
    -Dfile=lib-local/DarumaFrameworkJNA.jar \
    -Dpackaging=jar \
    -DlocalRepositoryPath=lib-local
